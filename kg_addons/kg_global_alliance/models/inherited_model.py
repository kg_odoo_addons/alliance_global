# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    @api.multi
    def _document_count(self):
        for each in self:
            document_ids = self.env['lead.document'].search([('lead_ref', '=', each.id)])
            each.document_count = len(document_ids)

    @api.multi
    def document_view(self):
        self.ensure_one()
        domain = [
            ('lead_ref', '=', self.id)]
        return {
            'name': _('Documents'),
            'domain': domain,
            'res_model': 'lead.document',
            'type': 'ir.actions.act_window',
            'view_id': False,
            'view_mode': 'tree,form',
            'view_type': 'form',
            'help': _('''<p class="oe_view_nocontent_create">
                               Click to Create for New Documents
                            </p>'''),
            'limit': 80,
            'context': "{'default_lead_ref': '%s'}" % self.id
        }

    document_count = fields.Integer(compute='_document_count', string='# Documents')

    lead_type = fields.Selection(string="Type", selection=[('trader', 'Trader'), ('manufacturer', 'Manufacturer')])
    source = fields.Char(string="Source")
    destination = fields.Char(string="Destination")
    target_price = fields.Float(string="Target Price")

    kg_sector_id = fields.Many2one("kg.sector", string="Sector")
    kg_division_id = fields.Many2one("kg.division", string="Division")
    kg_incoterm_id = fields.Many2one("kg.incoterm", string="Incoterm")
    kg_packingtype_id = fields.Many2one("kg.packingtype", string="Packing Type")

    payment_term_id = fields.Many2one('account.payment.term', string='Payment Terms')


class LeadDocument(models.Model):
    _name = 'lead.document'
    _rec_name = 'document_name'

    document_name = fields.Char(string='Document', required=True)
    description = fields.Text(string='Description', copy=False)
    lead_ref = fields.Many2one('crm.lead', invisible=1, copy=False)
    doc_attachment_id = fields.Many2many('ir.attachment', 'doc_attach_rel', 'doc_id', 'attach_id3', string="Attachment",
                                         help='You can attach the copy of your document', copy=False)


class KgSector(models.Model):
    _name = 'kg.sector'
    _rec_name = 'name'

    name = fields.Char(string="Sector")


class KgDivision(models.Model):
    _name = 'kg.division'
    _rec_name = 'name'

    name = fields.Char(string="Division")


class KgIncoterm(models.Model):
    _name = 'kg.incoterm'
    _rec_name = 'name'

    name = fields.Char(string="Incoterm")


class KgPackingType(models.Model):
    _name = 'kg.packingtype'
    _rec_name = 'name'

    name = fields.Char(string="Packing Type")


class KgPortOfLoading(models.Model):
    _name = 'kg.portofloading'
    _rec_name = 'name'

    name = fields.Char(string="Port Of Loading")


class KgAgent(models.Model):
    _name = 'kg.agent'
    _rec_name = 'name'

    name = fields.Char(string="Agent")


class KgShipment(models.Model):
    _name = 'kg.shipment'
    _rec_name = 'name'

    name = fields.Char(string="Shipment")


class ResUsers(models.Model):
    _inherit = 'res.users'

    code = fields.Char(string="Code")

    @api.multi
    def name_get(self):
        result = dict(super(ResUsers, self).name_get())
        records = self.browse(result.keys())
        for r in records:
            if r.code:
                result[r.id] += ' (' + r.code + ')'
        return result.items()

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        args = args or []
        recs = self.search([('code', operator, name)] + args, limit=limit)
        if not recs.ids:
            return super(ResUsers, self).name_search(name=name, args=args, operator=operator, limit=limit)
        return recs.name_get()


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def compute_total_commission(self):
        total_commission = 0.0
        for file in self:
            file.total_quantity = sum(line.product_uom_qty for line in file.order_line)
            total_commission = file.per_agent_commission * file.total_quantity
            file.total_commission = total_commission

    @api.multi
    def approve_quotation(self):
        for file in self:
            if file.is_dir_approved == False:
                file.is_dir_approved = True

    @api.one
    def kg_confirm_action(self):
        for file in self:
            if file.is_dir_approved == False:
                raise ValidationError("Director is not approved. So you can't proceed")
            else:
                file.action_confirm()

    kg_port_id = fields.Many2one("kg.portofloading", string="Port Of Loading")
    kg_agent_id = fields.Many2one("kg.agent", string="Agent")
    kg_shipment_id = fields.Many2one("kg.shipment", string="Shipment")

    kg_incoterm_id = fields.Many2one("kg.incoterm", string="Incoterm")
    bank_id = fields.Many2one("res.bank", string="Bank")

    per_agent_commission = fields.Float(string="Agent Commission Per Unit")
    total_commission = fields.Float(string="Total Agent Commission", compute='compute_total_commission')

    kg_remark = fields.Text(string="Remark")

    confirmed_by = fields.Char(string="Confirmed By Medium")
    po_number = fields.Char(string="PO Number")

    is_dir_approved = fields.Boolean("Director Approved")

    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ('swift_received', 'SO Swift Received'),
        ('finance_confirmed', 'Finance Confirmed'),
        ('doc_sen_and_waiting', 'Document sent and waiting for approval'),
        ('customer_confirmed', 'Customer confirmed and despach request to admin'),
        ('dispatched', 'Dispatched'),
        ('completed', 'Completed')], string='Status', readonly=True, copy=False,
        index=True, track_visibility='onchange',
        default='draft')

    @api.multi
    def b_swift_received(self):
        self.write({'state': 'swift_received'})

    @api.multi
    def b_finance_confirmed(self):
        self.write({'state': 'finance_confirmed'})

    @api.multi
    def b_doc_sen_and_waiting(self):
        self.write({'state': 'doc_sen_and_waiting'})

    @api.multi
    def b_customer_confirmed(self):
        self.write({'state': 'customer_confirmed'})

    @api.multi
    def b_dispatched(self):
        self.write({'state': 'dispatched'})

    @api.multi
    def b_completed(self):
        self.write({'state': 'completed'})


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    country_id = fields.Many2one("res.country", string="Country Of Origin")
    kg_packingtype_id = fields.Many2one("kg.packingtype", string="Packing Type")


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    kg_incoterm_id = fields.Many2one("kg.incoterm", string="Incoterm")

    is_dir_approved = fields.Boolean("Director Approved")

    @api.multi
    def approve_quotation(self):
        for file in self:
            if file.is_dir_approved == False:
                file.is_dir_approved = True

    @api.one
    def kg_confirm_action(self):
        for file in self:
            if file.is_dir_approved == False:
                raise ValidationError("Director is not approved. So you can't proceed")
            else:
                file.button_confirm()
                group_obj = self.env.ref('stock.group_stock_manager')
                partners = []
                subtype_ids = self.env['mail.message.subtype'].search(
                    [('res_model', '=', 'purchase.order')]).ids
                subtype_ids.append(1)
                subtype_ids.append(2)
                for user in group_obj.users:
                    self.message_subscribe(partner_ids=[user.partner_id.id], subtype_ids=subtype_ids)
                    partners.append(user.partner_id.id)
                body = _(u'New Purchase Order Created ' + file.name + '.')
                file.message_post(body=body, partner_ids=partners)


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    kg_vendor_id = fields.Many2one("res.partner", string="Shipping Vendor")
    shipping_charge = fields.Float(string="Shipping Charge Per Container")
    no_of_containers = fields.Float(string="No Of Containers")
    total_shipping_charge = fields.Float(string="Total Shipping Charge", compute='cal_total_shipping_charge')

    @api.multi
    def cal_total_shipping_charge(self):
        for file in self:
            if file.shipping_charge:
                file.total_shipping_charge = file.shipping_charge * file.no_of_containers

    kg_custom_clearance = fields.Float(string="Custom Clearance")
    inspection_charge = fields.Float(string="Inspection Charge")
    other_charge = fields.Float(string="Other Charge")
    sea_freight = fields.Float(string="Sea Freight Charge")
    sea_freight_other = fields.Float(string="Sea Freight Charge (Other)")
    detention = fields.Float(string="Demurrage & Detention Charge")
    local_transportation = fields.Float(string="Local Transportation Charge")

    kg_remark = fields.Text(string="Remark")
    kg_bill_no = fields.Char(string="Bill No")

    kg_invoice_id = fields.Many2one('account.invoice', string='Invoice')

    @api.multi
    def create_invoice_from_delivery(self):
        if self.kg_invoice_id and self.kg_invoice_id.id:
            raise UserError(_('Invoice Already created'))
        if self.picking_type_code != 'outgoing':
            raise UserError(_('this option only for delivery orders'))
        sale_order_obj = self.sale_id
        print '////////////////////////////////////////////'
        print '////////////////////////////////////////////'
        print '////////////////////////////////////////////'
        print '////////////////////////////////////////////', sale_order_obj
        print '////////////////////////////////////////////'
        print '////////////////////////////////////////////'
        print '////////////////////////////////////////////'
        print '////////////////////////////////////////////'
        result = sale_order_obj.action_invoice_create(grouped=False)
        print result
        self.kg_invoice_id = result and result[0] or False
        if self.kg_invoice_id and self.kg_invoice_id.id:
            self.kg_invoice_id.action_invoice_open()
            self.kg_invoice_id.kg_do_id = self.id
            inv_obj = self.kg_invoice_id
            inv_obj.write(
                {'kg_custom_clearance': self.kg_custom_clearance, 'inspection_charge': self.inspection_charge,
                 'other_charge': self.other_charge, 'sea_freight': self.sea_freight,
                 'sea_freight_other': self.sea_freight_other, 'detention': self.detention,
                 'local_transportation': self.local_transportation})
        return True


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    kg_custom_clearance = fields.Float(string="Custom Clearance")
    inspection_charge = fields.Float(string="Inspection Charge")
    other_charge = fields.Float(string="Other Charge")
    sea_freight = fields.Float(string="Sea Freight Charge")
    sea_freight_other = fields.Float(string="Sea Freight Charge (Other)")
    detention = fields.Float(string="Demurrage & Detention Charge")
    local_transportation = fields.Float(string="Local Transportation Charge")

    @api.depends('origin')
    def _compute_sale_order_id(self):
        for file in self:
            if file.origin:
                sale_order = self.env['sale.order'].search([('name', '=', file.origin)])
                if sale_order:
                    file.kg_sale_order_id = sale_order.id

    kg_sale_order_id = fields.Many2one('sale.order', string='Sale Order', compute='_compute_sale_order_id')

    @api.depends('number')
    def _compute_picking_id(self):
        for file in self:
            if file.number:
                picking_order = self.env['stock.picking'].search([('kg_invoice_id', '=', file.number)])
                if picking_order:
                    file.kg_picking_id = picking_order.id

    kg_picking_id = fields.Many2one('stock.picking', string='Stock Picking', compute='_compute_picking_id')
