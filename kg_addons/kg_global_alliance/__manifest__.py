# -*- coding: utf-8 -*-
{
    'name': 'KG Global Alliance Customizations',
    'version': '10.1',
    'description': """KG, Global Alliance Customizations""",
    'summary': """Global Alliance Customizations""",
    'category': 'Sales',
    'license': 'LGPL-3',
    'author': "Muhammad Faizal NS",
    'website': "http://klystrontech.com/",
    'depends': ['crm', 'sale', 'purchase', 'account', 'stock'],
    'data': [
        'security/kg_security.xml',
        'security/ir.model.access.csv',
        'views/inherited_views.xml',
        'report/report_view.xml',
        'report/report_proform.xml',
        'report/report_quotation.xml',
        'report/report_packaging_list_1.xml',
        'report/report_packaging_list_2.xml',
        'report/report_commercial_invoice.xml',
        'report/report_shipping_application.xml',
    ],
    'qweb': [],
    'installable': True,
    'application': True,
}
