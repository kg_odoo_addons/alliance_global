from odoo import models, fields, api
from odoo.tools.translate import _
from odoo import SUPERUSER_ID
from odoo import models, fields, api
from odoo import exceptions, _
from odoo.exceptions import Warning
import datetime as dt
from datetime import  timedelta, tzinfo, time, date, datetime
from dateutil.relativedelta import relativedelta 
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import Warning
import xlwt
from odoo.exceptions import UserError

from xlsxwriter.workbook import Workbook
#from cStringIO import StringIO
from io import StringIO
import io
import base64
from odoo.exceptions import UserError

class VatWizardPO(models.TransientModel):
    _name = 'kg.vat.wizard.po'
    _description = 'kg.vat.wizard.po'
    
 
                
    
    
    def print_excel_report(self):
		
        workbook= xlwt.Workbook(encoding="UTF-8")
        company_id = self.company_id and self.company_id.id
#        if company_id in (1,3):
#			raise UserError(_('no vat deifned for this company'))

        account_id = 3
        date_from = self.date_from

        date_to = self.date_to

        if date_from > date_to:
		
			raise UserError(_('invalid dates'))


        move_ids = self.env['account.move'].search([('date','>=',date_from),('date','<=',date_to)])

        print "111111111111111111111111",move_ids
        move_list = []
        for mo in move_ids:
			move_list.append(mo.id)

        move_line_ids = self.env['account.move.line'].search([('account_id','=',account_id),('move_id','in',move_list)])
		

        filename= 'VAT Report.xls'
        vat_period=datetime.strptime(date_from, '%Y-%m-%d').strftime('%d/%m/%Y')+"\tto\t"+datetime.strptime(date_to, '%Y-%m-%d').strftime('%d/%m/%Y')
		
        report_date = date.today()		
        report_date=report_date.strftime("%d/%m/%Y")	
        workbook= xlwt.Workbook(encoding="UTF-8")		
		
        xlwt.add_palette_colour("custom_colour", 0x10)	
        workbook.set_colour_RGB(0x10,178, 190, 181)
		#For coloured cells
		
        filename='VatRegister.xls'
        sheet= workbook.add_sheet('VAT Report',cell_overwrite_ok=True)		

		
        style_coloured = xlwt.easyxf('font:name calibri,height 200;align: horiz center, vert center;pattern: fore_colour custom_colour,pattern solid;borders:top_color black, bottom_color black, right_color black, left_color black, top thin,right thin,bottom thin,left thin;pattern: fore_colour custom_colour,pattern solid')
        style_title= xlwt.easyxf('font:name calibri,height 200;align: horiz left, vert center;')		
        style_left= xlwt.easyxf('font:name calibri,height 200;align: horiz left, vert center;borders:top_color black, bottom_color black, right_color black, left_color black, top thin,right thin,bottom thin,left thin;')
		
        style_right=xlwt.easyxf('font:name calibri,height 200;align: horiz right, vert center;borders:top_color black, bottom_color black, right_color black, left_color black, top thin,right thin,bottom thin,left thin;')		
		
        style_centre=xlwt.easyxf('align: horiz center, vert center;')	

        style_filter=xlwt.easyxf('font:name calibri,height 200;align: horiz center, vert center;borders:top_color black, bottom_color black,top thin,bottom thin;pattern: fore_colour custom_colour,pattern solid')	
        style_grey=xlwt.easyxf('pattern:fore_colour custom_colour,pattern solid')		
        row = 0			
        col =0		
		
		
  	

        header=['Sr.No','Vendor Name','Vendor TRN','Invoice Date','Invoice Number'
				,'Purchase value AED Excluding VAT','Value of VAT','Tax Code wise reporting STD-P(Standard Rate @5%) ZRO-P(Zero Rated Supply) RCM-IG-S(Purchase from GCC)EXM-P(Exempt Purchase) OS(Out of scope) STD-IR(Purchase with no ITC) IA(Incase of tax credit not issued)','In case of Import GCC wise report needs to be generated Kuwait, Bahrain, Oman, Qatar, Saudi']
		
        for i in range(0,9):
			sheet.write(row,col,header[i],style_coloured)
			col=col+1
        count = 0
        for mvl in move_line_ids:
			count=count+1
			row=row+1
        
			col=0			
			sheet.write(row,col,count,style_right)		
			col=col+1
			partner = mvl.partner_id and mvl.partner_id.name	
			sheet.write(row,col,partner,style_left)	

			col=col+1
			tr_no = mvl.partner_id and mvl.partner_id.kg_trno
			sheet.write(row,col,tr_no,style_left)

			col=col+1
			invoice_date = mvl.move_id and mvl.move_id.date
			sheet.write(row,col,invoice_date,style_right)
			
			invoice_number = mvl.move_id and mvl.move_id.name				
				
			col=col+1				
			sheet.write(row,col,invoice_number,style_right)				
			invoice_obj = self.env['account.invoice'].search([('number','=',invoice_number)])			
			amount_untaxed = invoice_obj.amount_untaxed	
			amount_tax = invoice_obj.amount_tax		
			col=col+1	
			sheet.write(row,col,amount_untaxed,style_right)				
			col=col+1					
				
			sheet.write(row,col,amount_tax,style_right)				
			col=col+1	
			label = mvl.name				
			sheet.write(row,col,label,style_left)
			col=col+1				
			state_id = mvl.partner_id and mvl.partner_id.state_id and mvl.partner_id.state_id.name or ''
			sheet.write(row,col,state_id,style_left)			
				
			












			
				
				

        fp = io.BytesIO()
        workbook.save(fp)
        excel_file = base64.encodestring(fp.getvalue())
        self.write({'excel_file':excel_file,'file_name':filename})

        return {
              'view_type': 'form',
              "view_mode": 'form',
              'res_model': 'kg.vat.wizard.po',
              'res_id': self.id,
              'type': 'ir.actions.act_window',
              'target': 'new'
              }
    

    date_from = fields.Date(string="Date Start")
    date_to = fields.Date(string="Date End")
    excel_file = fields.Binary('Dowload Report Excel')
    file_name = fields.Char('Excel File',)   
    company_id = fields.Many2one('res.company',string="Company",default=lambda self: self.env.user.company_id)
