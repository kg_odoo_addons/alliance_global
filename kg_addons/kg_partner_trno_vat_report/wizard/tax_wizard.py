from odoo import models, fields, api
from odoo.tools.translate import _
from odoo import SUPERUSER_ID
from odoo import models, fields, api
from odoo import exceptions, _
from odoo.exceptions import Warning
import datetime as dt
from datetime import  timedelta, tzinfo, time, date, datetime
from dateutil.relativedelta import relativedelta 
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import Warning
import xlwt
from odoo.exceptions import UserError

from xlsxwriter.workbook import Workbook
#from cStringIO import StringIO
from io import StringIO
import io
import base64
from odoo.exceptions import UserError

class kg_tax_wizard(models.TransientModel):
    
    _name = 'kg.tax.wizard'
    
    date_from = fields.Date(string="Date Start")
    date_to = fields.Date(string="Date End")
    company_id = fields.Many2one('res.company',string="Company",default=lambda self: self.env.user.company_id)
    salesperson_id = fields.Many2one('res.users', string='Salesperson', default=lambda self: self.env.user.id)
    
    
    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['salesperson_id','company_id', 'date_from', 'date_to'])[0]
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['salesperson_id','company_id', 'date_from', 'date_to'])[0])
        return self.env['report'].get_action(self, 'kg_partner_trno_vat_report.report_taxpay', data=data)