# -*- coding: utf-8 -*-
# Date:25-04-18
#Author :Ashish Thomas
import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging


class PurchaseOrder(models.Model):
    
    _inherit='purchase.order'
    
    
    tax_id_purchase = fields.Many2one('account.tax','Transaction')
    
    kg_kind_pur = fields.Selection([('capital','Capital Goods'),('non_cap','Non Capital Goods')],'Kind of Purchase',default='non_cap')
    
    kg_rev_charges  = fields.Boolean('Reverse Charge Applicable')
    @api.multi
    def action_view_invoice(self):
        '''
        This function returns an action that display existing vendor bills of given purchase order ids.
        When only one found, show the vendor bill immediately.
        '''
        action = self.env.ref('account.action_invoice_tree2')
        result = action.read()[0]

        #override the context to get rid of the default filtering
        result['context'] = {'type': 'in_invoice', 'default_purchase_id': self.id}
        result['context']['default_tax_id_purchase'] = self.tax_id_purchase.id
        result['context']['default_kg_kind_pur'] = self.kg_kind_pur
        result['context']['default_kg_rev_charges'] = self.kg_rev_charges

        if not self.invoice_ids:
            # Choose a default account journal in the same currency in case a new invoice is created
            journal_domain = [
                ('type', '=', 'purchase'),
                ('company_id', '=', self.company_id.id),
                ('currency_id', '=', self.currency_id.id),
            ]
            default_journal_id = self.env['account.journal'].search(journal_domain, limit=1)
            if default_journal_id:
                result['context']['default_journal_id'] = default_journal_id.id
        else:
            # Use the same account journal than a previous invoice
            result['context']['default_journal_id'] = self.invoice_ids[0].journal_id.id

        #choose the view_mode accordingly
        if len(self.invoice_ids) != 1:
            result['domain'] = "[('id', 'in', " + str(self.invoice_ids.ids) + ")]"
        elif len(self.invoice_ids) == 1:
            res = self.env.ref('account.invoice_supplier_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = self.invoice_ids.id
        return result
    
    
    
    @api.model
    def create(self, vals):
        res= super(PurchaseOrder, self).create(vals)
        
        dom_taxes = self.env['account.tax'].search([('name','ilike','Domestic'),('type_tax_use','=','purchase')]).ids
        print dom_taxes,'ccccccccccc'
        if res.tax_id_purchase.id in dom_taxes:
        
            if not res.partner_id.country_id:
                raise UserError(_('Define Country column in vendor master'))
            
            if res.partner_id.country_id.phone_code != 971:
                raise UserError(_('Vendor country and selected transaction are not matching'))
            
            
            if res.partner_id.state_id.country_id.phone_code != 971:
                raise UserError(_('Vendor state and selected transaction are not matching'))
                
            
            if not res.partner_id.state_id:
                raise UserError(_('Define state column in vendor master'))
            
            
        
        
        extra_taxes = self.env['account.tax'].search([('name','ilike','Extra GCC'),('type_tax_use','=','purchase')]).ids
        print extra_taxes,'ccccccccccc'
        if res.tax_id_purchase.id in extra_taxes:
        
            if not res.partner_id.country_id:
                raise UserError(_('Define Country column in vendor master'))
            
            if res.partner_id.country_id.kg_region != 'inside':
                raise UserError(_('Vendor country and selected transaction are not matching'))
            
            
            if res.partner_id.state_id.country_id.phone_code == 971:
                raise UserError(_('Vendor state and selected transaction are not matching'))
                
            
            if not res.partner_id.state_id:
                raise UserError(_('Define state column in vendor master'))
            
            
        import_taxes = self.env['account.tax'].search([('name','ilike','Import'),('type_tax_use','=','purchase')]).ids
        print import_taxes,'ccccccccccc'
        if res.tax_id_purchase.id in import_taxes:
        
            if not res.partner_id.country_id:
                raise UserError(_('Define Country column in vendor master'))
            
            if res.partner_id.country_id.kg_region != 'outside':
                raise UserError(_('Vendor country and selected transaction are not matching'))
            
            
            if res.partner_id.state_id.country_id.phone_code == 971:
                raise UserError(_('Vendor state and selected transaction are not matching'))
                
            
            if not res.partner_id.state_id:
                raise UserError(_('Define state column in vendor master'))
        return res
    
    
    
    @api.multi
    def write(self, vals):
        print vals,'111'
        res = super(PurchaseOrder, self).write(vals)
        print res
        dom_taxes = self.env['account.tax'].search([('name','ilike','Domestic'),('type_tax_use','=','purchase')]).ids
        print dom_taxes,'wwwwwwwwwww'
        print self.tax_id_purchase.id
        
        if self.tax_id_purchase.id in dom_taxes:
        
            if not self.partner_id.country_id:
                raise UserError(_('Define Country column in vendor master'))
            
            if self.partner_id.country_id.phone_code != 971:
                raise UserError(_('Vendor country and selected transaction are not matching'))
            
            
            if self.partner_id.state_id.country_id.phone_code != 971:
                raise UserError(_('Vendor state and selected transaction are not matching'))
                
            
            if not self.partner_id.state_id:
                raise UserError(_('Define state column in vendor master'))
            
        extra_taxes = self.env['account.tax'].search([('name','ilike','Extra GCC'),('type_tax_use','=','purchase')]).ids
        print extra_taxes,'ccccccccccc'
        if self.tax_id_purchase.id in extra_taxes:
        
            if not self.partner_id.country_id:
                raise UserError(_('Define Country column in vendor master'))
            
            if self.partner_id.country_id.kg_region != 'inside':
                raise UserError(_('Vendor country and selected transaction are not matching'))
            
            
            if self.partner_id.state_id.country_id.phone_code == 971:
                raise UserError(_('Vendor state and selected transaction are not matching'))
                
            
            if not self.partner_id.state_id:
                raise UserError(_('Define state column in vendor master'))
            
        import_taxes = self.env['account.tax'].search([('name','ilike','Import'),('type_tax_use','=','purchase')]).ids
        print import_taxes,'ccccccccccc'
        if self.tax_id_purchase.id in import_taxes:
        
            if not self.partner_id.country_id:
                raise UserError(_('Define Country column in vendor master'))
            
            if self.partner_id.country_id.kg_region != 'outside':
                raise UserError(_('Vendor country and selected transaction are not matching'))
            
            
            if self.partner_id.state_id.country_id.phone_code == 971:
                raise UserError(_('Vendor state and selected transaction are not matching'))
                
            
            if not self.partner_id.state_id:
                raise UserError(_('Define state column in vendor master'))
        return res

    
    