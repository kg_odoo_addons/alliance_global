# -*- coding: utf-8 -*-
# Date:25-04-18
#Author :Ashish Thomas
import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging


class kg_partner(models.Model):
    
    _inherit = "res.partner"
    
    kg_trno =fields.Char('TR No')
    
    _sql_constraints = [
        ('kg_trno_uniq', 'unique(kg_trno)', 'TR No must be unique !'),
    ]

    
