# -*- coding: utf-8 -*-
# Date:25-04-18
#Author :Ashish Thomas
import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging

class account_move(models.Model):
    
    _inherit='account.move'
    
    kg_rev_charges  = fields.Boolean('Reverse Charge Applicable')
    
    kg_org_move_id  = fields.Many2one('account.move','original Move')
    
    
    kg_rev_entry  = fields.Many2one('account.move','Reversed Entry')
    
    kg_dep_entry  = fields.Many2one('account.move','Dependant Entry')
    
  
    
    kg_rev_count = fields.Integer('Reverse Count',compute='compute_rev_count')
    
    @api.multi
    def compute_rev_count(self):
        for record in self:
            rev_obj = self.env['account.move']
            
            record.kg_rev_count = rev_obj.search_count([('kg_org_move_id', '=',record.id)]) 