# -*- coding: utf-8 -*-
# Date:25-04-18
#Author :Ashish Thomas
import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging

class account_invoice(models.Model):
    
    _inherit = 'account.invoice'
    
    tax_id_sale =fields.Many2one('account.tax','Transaction')
    
    tax_id_purchase = fields.Many2one('account.tax','Transaction')
    
    kg_kind_pur = fields.Selection([('capital','Capital Goods'),('non_cap','Non Capital Goods')],'Kind of Purchase',default='non_cap')

    
    kg_rev_charges  = fields.Boolean('Reverse Charge Applicable')
    
    kg_rev_entry  = fields.Many2one('account.move','Reverse Entry')
    
    
    @api.multi
    def create_rev_entry(self):
        for record in self:
            kg_vat_rev_acc = self.env.ref('kg_partner_trno_vat_report.kg_vat_rev_acc')
            kg_vat_inp_tax_acc = self.env.ref('kg_partner_trno_vat_report.kg_vat_inp_tax_acc')
                
            rev_amt = abs(record.amount_total*0.05)
            print rev_amt,'111111111111'
            line=[]    
            new_lines={
                    
                    'analytic_account_id':False,
                    'name':kg_vat_rev_acc.name,
                    'invoice_id':record.id,
                    'credit':rev_amt,
                    'debit':False,
                     'account_id':kg_vat_rev_acc.id,
                    'partner_id':record.partner_id.id
                    
                    
                    
                    }
            a=(0,0,new_lines)
            line.append(a)
                
            new_lines1={
                    
                    'analytic_account_id':False,
                    'name':kg_vat_inp_tax_acc.name,
                    
                    'invoice_id':record.id,
                    'debit':rev_amt,
                    'credit':False,
                    'account_id':kg_vat_inp_tax_acc.id,
                   
                    'partner_id':record.partner_id.id
                    
                    
                    
                    }
            b=(0,0,new_lines1)
            line.append(b)
            print line,''
            date = record.date or record.date_invoice
            move_vals = {
                'ref': record.reference,
                'line_ids': line,
                'journal_id': record.journal_id.id,
                'date': date,
                'narration': record.comment,
            }
            
            rev_move = self.env['account.move'].create(move_vals)
            rev_move.post()
            record.write({'kg_rev_entry':rev_move.id})
            return True
            
    
    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)

            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total, inv.date_invoice)[0]
                res_amount_currency = total_currency
                ctx['date'] = inv._get_currency_rate_date()
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })
            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)
           # print line,'lnessssssssssss'
            if inv.tax_id_purchase.name =='Imports Taxable' and inv.kg_rev_charges == True:
                kg_vat_rev_acc = self.env.ref('kg_partner_trno_vat_report.kg_vat_rev_acc')
                kg_vat_inp_tax_acc = self.env.ref('kg_partner_trno_vat_report.kg_vat_inp_tax_acc')
                
                rev_amt = abs(total*0.05)
                print rev_amt,'111111111111'
                
                new_lines={
                    
                    'analytic_account_id':False,
                    'name':kg_vat_inp_tax_acc.name,
                    'invoice_id':inv.id,
                    'credit':rev_amt,
                    'debit':False,
                    'account_id':kg_vat_inp_tax_acc.id,
                    'partner_id':inv.partner_id.id
                    
                    
                    
                    }
                a=(0,0,new_lines)
                line.append(a)
                
                new_lines1={
                    
                    'analytic_account_id':False,
                    'name':kg_vat_rev_acc.name,
                    'invoice_id':inv.id,
                    'debit':rev_amt,
                    'credit':False,
                    'account_id':kg_vat_rev_acc.id,
                    'partner_id':inv.partner_id.id
                    
                    
                    
                    }
                b=(0,0,new_lines1)
                line.append(b)
                print line,''
            date = inv.date or inv.date_invoice
            move_vals = {
                'ref': inv.reference,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            
            
            
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
        return True