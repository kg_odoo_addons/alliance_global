# -*- coding: utf-8 -*-
# Date:25-04-18
#Author :Ashish Thomas
import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp
import logging



class account_payment(models.Model):
    
    _inherit = 'account.payment'
    
    kg_advance = fields.Boolean('Advance')
    
    
    kg_sale_id = fields.Many2one('sale.order','Sale Order')
    
    kg_vat = fields.Float('Vat Amount')
    
    
    @api.onchange('amount')
    def onchange_amount(self):
        for record in self:
            record.kg_vat =(record.amount)*0.05