# -*- coding: utf-8 -*-

import time
from odoo import api, models
from dateutil.parser import parse
from odoo.exceptions import UserError


class ReportTax(models.AbstractModel):
    _name = 'report.kg_partner_trno_vat_report.report_taxpay'
    
    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        sales_records = []
        orders = self.env['sale.order'].search([('user_id', '=', docs.salesperson_id.id)])
        if docs.date_from and docs.date_to:
            for order in orders:
                if parse(docs.date_from) <= parse(order.date_order) and parse(docs.date_to) >= parse(order.date_order):
                    sales_records.append(order);
        else:
            raise UserError("Please enter duration")
        
        
        
        dom_sale_taxes = self.env['account.tax'].search([('name','ilike','Domestic'),('type_tax_use','=','sale')])
        
        print dom_sale_taxes,'1111111111'
        
        dom_tax_lines=[]
        dom_sum=0
        dom_tax=0
        dom=[]
        
        for tax in dom_sale_taxes:
            print 'inside loooooooooooooooop'
            
            
            
           
            
            self.env.cr.execute("""select res_country_state.name,sum(account_invoice.amount_untaxed),sum(account_invoice.amount_untaxed*(account_tax.amount/100.0)) as tax,2 as level
from   account_invoice


join   res_partner
on     account_invoice.partner_id = res_partner.id

join   res_country_state
on     res_country_state.id = res_partner.state_id

join   account_tax
on     account_tax.id = account_invoice.tax_id_sale

where  account_invoice.type='out_invoice' and account_invoice.tax_id_sale =%s and account_invoice.date_invoice >= %s and account_invoice.date_invoice<=%s


group by res_country_state.name""", (tax.id,docs.date_from,docs.date_to))
            
            results = self.env.cr.dictfetchall()
            print results,'22222222222222222'
            c=[]
            if results:
                a={}
                b=[]
                self.env.cr.execute("""select sum(amount_untaxed) from account_invoice
                where type='out_invoice' and tax_id_sale =%s and date_invoice >= %s and date_invoice<=%s""",(tax.id,docs.date_from,docs.date_to))
                
                amt_tot=self.env.cr.fetchone()[0]
                
                a['name']=(tax.name).rjust(10)
                a['sum']=amt_tot
                a['level']=1
                tax_sum=0.0
                for i in results:
                    tax_sum+=i['tax']
                a['tax']=tax_sum
                dom_sum+=amt_tot
                dom_tax+=tax_sum
                
                
                
                b.append(a)
                
                
                
                c=b+results
                
            dom_tax_lines=dom_tax_lines+c
            
        d={}
        d['name']='Local Supplies'
        d['sum']=dom_sum
        d['tax']=dom_tax
        d['level']=0
        dom.append(d)
        dom_tax_lines=dom+dom_tax_lines
        
        print dom_tax_lines,'dom_tax_linesdom_tax_linesdom_tax_linesdom_tax_lines'
        
        
        
        
        ###intra gcc supplis
        intra_gcc_id = self.env['account.tax'].search([('name','=','Intra GCC Supplies'),('type_tax_use','=','sale')])
        print intra_gcc_id,'intra_gcc_idintra_gcc_idintra_gcc_id'
        intra_lines=[]
        if intra_gcc_id:
            self.env.cr.execute("""select sum(amount_untaxed) from account_invoice
                    where type='out_invoice' and tax_id_sale =%s and date_invoice >= %s and date_invoice<=%s""",(intra_gcc_id.id,docs.date_from,docs.date_to))
                    
            amt_tot=self.env.cr.fetchone()[0]
            print amt_tot,'amt_totamt_totamt_totamt_totamt_tot'
            
            if amt_tot:
                a={}
                
                a['name']='GCC Supplies'
                
                a['sum']=amt_tot
                
                a['level']=0
                
                a['tax']=0
                
                intra_lines.append(a)
                
                a={}
                
                a['name']=intra_gcc_id.name
                
                a['sum']=amt_tot
                
                a['level']=1
                a['tax']=0
                
                intra_lines.append(a)
        
        dom_tax_lines=dom_tax_lines+intra_lines
        print dom_tax_lines,'111111111111111'
        
        
        ###intra gcc supplis
        out_gcc_id = self.env['account.tax'].search([('name','=','Exports'),('type_tax_use','=','sale')])
        print out_gcc_id,'intra_gcc_idintra_gcc_idintra_gcc_id'
        out_lines=[]
        if out_gcc_id:
            self.env.cr.execute("""select sum(amount_untaxed) from account_invoice
                    where type='out_invoice' and tax_id_sale =%s and date_invoice >= %s and date_invoice<=%s""",(out_gcc_id.id,docs.date_from,docs.date_to))
                    
            amt_tot=self.env.cr.fetchone()[0]
            print amt_tot,'amt_totamt_totamt_totamt_totamt_tot'
            
            if amt_tot:
                a={}
                
                a['name']='Outside GCC Supplies'
                
                a['sum']=amt_tot
                
                a['level']=0
                
                a['tax']=0
                
                out_lines.append(a)
                
                a={}
                
                a['name']=out_gcc_id.name
                
                a['sum']=amt_tot
                
                a['level']=1
                a['tax']=0
                
                out_lines.append(a)
        
        dom_tax_lines=dom_tax_lines+out_lines
        print dom_tax_lines,'2222222222222222'
        
        
        
        
        ###advances receot
        
        
        self.env.cr.execute("""select res_country_state.name,sum(account_payment.amount),sum(account_payment.kg_vat) as tax,1 as level
from   account_payment


join   res_partner
on     account_payment.partner_id = res_partner.id

join   res_country_state
on     res_country_state.id = res_partner.state_id



where  account_payment.kg_advance=TRUE and account_payment.payment_type ='inbound' and account_payment.payment_date >= %s and account_payment.payment_date<=%s


group by res_country_state.name""", (docs.date_from,docs.date_to))
        
        
        results = self.env.cr.dictfetchall()
        print results,'resultsresultsresultsresults'
        
        
        self.env.cr.execute("""select res_country_state.name,sum(account_payment.amount),sum(account_payment.kg_vat) as tax,1 as level
from   account_payment

join sale_order
on   account_payment.kg_sale_id = sale_order.id

join account_invoice
on   sale_order.name = account_invoice.origin


join   res_partner
on     account_payment.partner_id = res_partner.id

join   res_country_state
on     res_country_state.id = res_partner.state_id



where  account_invoice.type='out_invoice'  and account_invoice.date_invoice >= %s and account_invoice.date_invoice<=%s and account_payment.kg_advance=TRUE and account_payment.payment_type ='inbound' 


group by res_country_state.name""", (docs.date_from,docs.date_to))
        
        
        result_new = self.env.cr.dictfetchall()
        
        print result_new,'result_newresult_newresult_newresult_new'
        
        if results and not result_new:
            results=results
        if results and result_new:
            for i in results:
                a=0
                for j in result_new:
                    if i['name'] == j['name']:
                        i['sum']=i['sum']-j['sum']
                        i['tax']=i['tax']-j['tax']
                        result_new.pop(a)
                    a=a+1
            for i in result_new:
                i['sum']=-i['sum']
                i['tax']=-i['tax']
            for i in result_new:
                results.append(i)
        if result_new and not results:
            for i in result_new:
                i['sum']=-i['sum']
                i['tax']=-i['tax']
            for i in result_new:
                results.append(i)
            
                    
        
        if results:
            adv_sum=0
            adv_tax=0
            adv=[]
            for i in results:
                adv_sum+=i['sum']
                adv_tax+=i['tax']
                
            aadv_a={}
            
            aadv_a['name']='Advance Receipt'
            aadv_a['sum']=adv_sum
            aadv_a['tax']=adv_tax
            aadv_a['level']=0
            adv.append(aadv_a)    
            
            dom_tax_lines=dom_tax_lines+adv+results
            
        print dom_tax_lines,'4444444444444444444444444444444444444'
        
        
        
        ###reverse charges
        
        self.env.cr.execute("""select sum(account_invoice.amount_untaxed),sum(account_invoice.amount_untaxed*(account_tax.amount/100.0)) as tax ,'Reverse Charges' as name,0 as level from account_invoice
        
        join   account_tax
on     account_tax.id = account_invoice.tax_id_purchase
where account_invoice.type='in_invoice' and account_invoice.kg_rev_charges =TRUE and account_invoice.date_invoice >= %s and account_invoice.date_invoice<=%s""",(docs.date_from,docs.date_to))
                    
        
        results = self.env.cr.dictfetchall()
        print results,'resulttttttttt'
        if results[0]['sum'] != None:
            dom_tax_lines=dom_tax_lines+results
        
        print dom_tax_lines,'333333333333333333333333333333333333'
        
        sum_supp=[]
        sales_tot=0
        sales_tax=0
        for i in dom_tax_lines:
          
            
            if i['level'] == 0:
                print i,'333333333333333'
                
                sales_tot+=i['sum']
                if i['tax']>0 or i['tax'] < 0 :
                    sales_tax+=i['tax']
                
        a={}
        a['name']='Total'
        a['sum']=sales_tot
        a['tax']=sales_tax
        a['level']=0
        sum_supp.append(a)
        dom_tax_lines=dom_tax_lines+sum_supp
        
        print dom_tax_lines,'55555555555555555555'
                
         
        ###########Purchases 
        
        purchase_tax_lines=[]
        
        
        dom_pur_taxes = self.env['account.tax'].search([('name','ilike','Domestic'),('type_tax_use','=','purchase')])
        
        print dom_pur_taxes,'1111111111'
        
       
        dom_pur_sum=0
        dom_pur_tax=0
        dom_pur=[]
        
        for tax in dom_pur_taxes:
            print 'inside looop'
            
            
         
            self.env.cr.execute("""select sum(account_invoice.amount_untaxed),sum(account_invoice.amount_untaxed*(account_tax.amount/100.0)) as tax ,1 as level from account_invoice
        
        join   account_tax
on     account_tax.id = account_invoice.tax_id_purchase
where account_invoice.type='in_invoice' and account_invoice.kg_kind_pur='non_cap'and account_invoice.tax_id_purchase =%s and account_invoice.date_invoice >= %s and account_invoice.date_invoice<=%s""",(tax.id,docs.date_from,docs.date_to))
                    
            results = self.env.cr.dictfetchall()
            print results,'resultsresultsresultsresultsresultsresultsresultsresultsresults'
            if results[0]['sum'] != None:
                print 'isnide loop1111'
                results[0]['name']=tax.name
                print results,'resultsresultsresults'
                purchase_tax_lines=purchase_tax_lines+results    
                for i in results:
                    dom_pur_tax+=i['tax']
                    
                    dom_pur_sum+=i['sum']
                    
                    
                    
                
                
                
            
            
        d={}
        d['name']='Local Purchases'
        d['sum']=dom_pur_sum
        d['tax']=dom_pur_tax
        d['level']=0
        dom_pur.append(d)
        purchase_tax_lines=dom_pur+purchase_tax_lines
        
        print purchase_tax_lines,'dom_tax_linesdom_tax_linesdom_tax_linesdom_tax_lines'
        
        
        
        out_gcc_pur_id = self.env['account.tax'].search([('name','=','Imports Taxable'),('type_tax_use','=','purchase')])
        print out_gcc_pur_id,'out_gcc_pur_idout_gcc_pur_idout_gcc_pur_id'
        out_pur_lines=[]
        if out_gcc_pur_id:
            self.env.cr.execute("""select sum(amount_untaxed) from account_invoice
                    where type='in_invoice' and tax_id_purchase =%s and date_invoice >= %s and date_invoice<=%s""",(out_gcc_pur_id.id,docs.date_from,docs.date_to))
                    
            amt_tot=self.env.cr.fetchone()[0]
            print amt_tot,'amt_totamt_totamt_totamt_totamt_tot'
            
            if amt_tot:
                a={}
                
                a['name']='Outside GCC Purchases'
                
                a['sum']=amt_tot
                
                a['level']=0
                
                a['tax']=0
                
                out_pur_lines.append(a)
                
                a={}
                
                a['name']=out_gcc_pur_id.name
                
                a['sum']=amt_tot
                
                a['level']=1
                a['tax']=0
                
                out_pur_lines.append(a)
        
        purchase_tax_lines=purchase_tax_lines+out_pur_lines
        
        
        
        print purchase_tax_lines,'111111111111111'
        
        
        dom_tax_pur_id = self.env['account.tax'].search([('name','ilike','Domestic'),('amount','>',0),('type_tax_use','=','purchase')])
        print dom_tax_pur_id,'dom_tax_pur_iddom_tax_pur_iddom_tax_pur_id'
        dom_tax_cap_pur_lines=[]
        if dom_tax_pur_id:
            self.env.cr.execute("""select sum(amount_untaxed) from account_invoice
                    where type='in_invoice' and account_invoice.kg_kind_pur='capital' and tax_id_purchase =%s and date_invoice >= %s and date_invoice<=%s""",(dom_tax_pur_id.id,docs.date_from,docs.date_to))
                    
            amt_tot=self.env.cr.fetchone()[0]
            print amt_tot,'amt_totamt_totamt_totamt_totamt_tot'
            
            if amt_tot:
                a={}
                
                a['name']='Capital Goods'
                
                a['sum']=amt_tot
                
                a['level']=0
                
                a['tax']=amt_tot*(dom_tax_pur_id.amount/100.0)
                
                dom_tax_cap_pur_lines.append(a)
                
                a={}
                
                a['name']=dom_tax_pur_id.name
                
                a['sum']=amt_tot
                
                a['level']=1
                a['tax']=amt_tot*(dom_tax_pur_id.amount/100.0)
                
                dom_tax_cap_pur_lines.append(a)
        
        purchase_tax_lines=purchase_tax_lines+dom_tax_cap_pur_lines
        
        
        
        ##Reversw cahrges
        self.env.cr.execute("""select 0 as sum,sum(account_invoice.amount_untaxed*(account_tax.amount/100.0)) as tax ,'Reverse Charges' as name,0 as level from account_invoice
        
        join   account_tax
on     account_tax.id = account_invoice.tax_id_purchase
where account_invoice.type='in_invoice' and account_invoice.kg_rev_charges =TRUE and account_invoice.date_invoice >= %s and account_invoice.date_invoice<=%s""",(docs.date_from,docs.date_to))
                    
        
        results = self.env.cr.dictfetchall()
        print results,'resulttttttttt'
        if results[0]['tax'] !=None:
            purchase_tax_lines=purchase_tax_lines+results
        
        print purchase_tax_lines,'dom_tax_linesdom_tax_linesdom_tax_linesdom_tax_lines'
        
        sum_purchases=[]
        purchases_tot=0
        purchases_tax=0
        for i in purchase_tax_lines:
          
            
            if i['level'] == 0:
                print i,'iiiiiiiiiii'
                purchases_tot+=i['sum']
                if i['tax']>0 or i['tax']< 0:
                    purchases_tax+=i['tax']
                
        a={}
        a['name']='Total'
        a['sum']=purchases_tot
        a['tax']=purchases_tax
        a['level']=0
        sum_purchases.append(a)
        purchase_tax_lines=purchase_tax_lines+sum_purchases
        
        
        a={}
        de=[]
        a['name']='Payable'
        a['sum']=''
        a['tax']=sales_tax-purchases_tax
        a['level']=0
        de.append(a)
        purchase_tax_lines=purchase_tax_lines+de
        
        
        
        
        
               
            
            
        
                    
            
           
            
         #   a['tax_val']=
         
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'orders':sales_records,
            'dom_tax_lines':dom_tax_lines,
             'purchase_tax_lines':purchase_tax_lines
        }
        return self.env['report'].render('kg_partner_trno_vat_report.report_taxpay', docargs)