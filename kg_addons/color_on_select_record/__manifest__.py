# -*- coding: utf-8 -*-
{
    'name': 'Color on Select Record',
    'version': '1.1',
    'description': """
                   Highlights Selected Records in Listview
                    """,
    'summary': """Highlights Selected Records in Listview""",
    'category': 'Web',
    'license': 'LGPL-3',
    'author': "Klystron Technolgies",
    'website': "http://klystrontech.com/",
    'depends': ['web'],
    'data': [
        'views/template.xml',

    ],

    'qweb': [],
    'installable': True,
    'application': True,
}
